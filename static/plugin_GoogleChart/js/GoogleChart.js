(function($) {
    $.fn.GoogleChart = function(target, chartStyle, chartType, packages, dataType, d, options) {
        if (typeof GoogleCharts === 'undefined') {
            GoogleCharts = {};
        }
        google.load("visualization", "1.1", {
            packages: packages,
            callback: drawChart
        });

        function drawChart() {
            if (dataType === 'remote') {
                console.log(d);
                var raw_data = $.ajax({
                    url: d,
                    dataType: "json",
                    async: false,
                }).responseText;
                d = $.parseJSON(raw_data);
            }
            var data = google.visualization.arrayToDataTable(d);
            if (chartStyle === 'material') {
                var chart = new google.charts[chartType](document.getElementById(target));
            }
            else {
                var chart = new google.visualization[chartType](document.getElementById(target));
            }
            chart.draw(data, options);
            // Return params for extensions
            GoogleCharts[target] = {};
            GoogleCharts[target].chart = chart;
            GoogleCharts[target].data = d;
            GoogleCharts[target].chartType = chartType;
            GoogleCharts[target].packages = packages;
            GoogleCharts[target].options = options;
        }

        $(window).resize(function() {
            drawChart();
        })
    }
}( jQuery));
