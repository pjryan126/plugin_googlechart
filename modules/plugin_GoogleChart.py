#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gluon import *
from gluon.contrib import simplejson as json

class Chart(object):

    def __init__(self, id, data, options):
        if not id:
            import uuid
            self.id = 'google-chart-%s' % uuid.uuid4()
        else:
            self.id = id
        if isinstance(data, list):
            self._dtype = 'array'
            self.data = data
        else:
            self._dtype = 'remote'
            self.data = '"%s"' % data

        if options:
            for key, value in options.items():
                if isinstance(value, bool):
                    options[key] = str(value).lower()
        self.options = options
        self.jsapi = SCRIPT(_type='text/javascript', _src='https://www.google.com/jsapi')
        self.js = SCRIPT(_type='text/javascript', 
                         _src=URL(c='static', f='plugin_GoogleChart/js/GoogleChart.js'))
        self.script = SCRIPT("""
            jQuery(document).ready(function($) {
                $('#%s').GoogleChart("%s", "%s", "%s", %s, "%s", %s, %s);
                });""" % \
                (self.id, self.id, self._cstyle, self.chart_type, 
                 self.packages, self._dtype, self.data, self.options))

    def draw(self):
        wrapper = DIV(_class='plugin-google-chart')
        widget = DIV(_id=self.id)
        wrapper.append(widget)
        wrapper.append(self.jsapi)
        wrapper.append(self.js)
        wrapper.append(self.script)
        return wrapper

class BarChart(Chart):

    def __init__(self, id=None, data=[], options=None, chart_style='classic'):
        
        if chart_style == 'classic':
            self.chart_type = 'BarChart'
            self._cstyle = 'classic'
            self.packages = ['corechart']
        else:
            self.chart_type = 'Bar'
            self._cstyle = 'material'
            self.packages = ['bar']
        super(BarChart, self).__init__(id=id, data=data, options=options)
